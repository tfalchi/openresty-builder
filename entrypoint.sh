#!/bin/bash
 
# Eseguiamo getopt sugli argomenti passati al programma, identificati con il carattere speciale $@
PARSED_OPTIONS=$(getopt -n "$0"  -o hb: --long "help,bash"  -- "$@")
 
#argomenti errati, qualcosa è andato storto con il comando getopt.
if [ $? -ne 0 ];
then
  exit 1
fi
 
# Una piccola magia da usare con il comando getopt
eval set -- "$PARSED_OPTIONS"
 
 
# Ora passa attraverso tutte le opzioni con un case e utilizza shift per analizzare 1 argomento alla volta
# $1 identifica il primo argomento, e quando usiamo shift scartiamo il primo argomento, quindi $2 diventa $1 e passa di nuovo attraverso il case.
while true;
do
  case "$1" in
 
    -h|--help)
      echo -e "usage:\tstart container (docker run -d --name openrestybuild -t falchit/openresty-builder)\n      \tget builded rpm (docker cp openrestybuild:/root/rpmbuild/RPMS/ ./)\n\n      \t--help show this message\n      \t--bash start an internal shell (for debugging purpose)"
     break;;
    -b|--bash)
       /bin/bash
       break;;
    --)
      shift
      break;;
    \?)
      echo "Invalid option. Try --help"
      break;;
  esac
done
