#INFO:
This dockerfile build 1.9.7.3 version of [openresty](https://openresty.org/) statically linked with openssl [1.0.2f](https://www.openssl.org/source/)

The relative container is builded every push on [falchit/openresty-builder](https://hub.docker.com/r/falchit/openresty-builder/)

###USAGE:
```bash
docker run -d --name openrestybuild -t falchit/openresty-builder
docker cp openrestybuild:/root/rpmbuild/RPMS/ ./
```
